/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.MethodReferenceExperiment;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rod
 */
public class Point {
    
    public int x;
    public int y;
    
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    // NOTE: this constructor is NOT something you would normally implement.
    // It makes a new Point with the x and y values swapped from the given
    // point.  We use it in the example below to demonstrate the use of a
    // constructor method reference.
    public Point(Point other) {
        this.x = other.y;
        this.y = other.x;
    }
    
    // All the compare methods below first compare the x values of two points
    // and then the y.  So for example, a sorted list of Points might look like
    // this:  (1,1), (1,2), (2,1), (2,2), ...
    // The methods return 0 for points that are equal, -1 when p1 < p2 and
    // +1 when p1 > p2.  
    // Methods of this type are used by many sorting methods in Java to make 
    // comparisons between elements in a list to decide where in the final
    // sorted list the elements should go
    public int compareTo(Point other) {
        if ( other.x == x ) {
            if ( other.y == y ) { return 0; }
            else { return other.y > y ? -1 : 1; }
        } else {
            return other.x > x ? -1 : 1;
        }
    }
    
    public static int compare(Point p1, Point p2) {
        if ( p2.x == p1.x ) {
            if ( p2.y == p1.y ) { return 0; }
            else { return p2.y > p1.y ? -1 : 1; }
        } else {
            return p2.x > p1.x ? -1 : 1;
        }
    }
    
    // All the reflect methods below simply swap the x and y attributes of a
    // point.
    public Point reflect() {
        return new Point(y, x);
    }
    
    public static Point staticReflect(Point p) {
        return new Point(p.y, p.x);
    }
    
    // Here we have a couple static nested classes that we use to demonstrate
    // the use of object instance method references further below
    static private class PointComparer {
        public int compare(Point p1, Point p2) {
            if ( p2.x == p1.x ) {
                if ( p2.y == p1.y ) { return 0; }
                else { return p2.y > p1.y ? -1 : 1; }
            } else {
                return p2.x > p1.x ? -1 : 1;
            }
        }
    }
    
    static private class PointReflecter {
        public Point reflect(Point p) {
            return new Point(p.y, p.x);
        }
    }
    
    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }
    
    public static void main(String[] args) {
        
        // First, let's make a list...
        List<Point> L = new ArrayList<>();
        
        // ...and add some random points to it
        L.add(new Point(3,8));
        L.add(new Point(1,8));
        L.add(new Point(1,9));
        L.add(new Point(3,7));
        L.add(new Point(2,8));
        L.add(new Point(2,7));
        L.add(new Point(1,9));
        L.add(new Point(3,7));
        L.add(new Point(3,6));
       
        
        // In the following four sets of lines, we do the same map, sort,
        // and print action using four different types of method references.  
        // The methods being referred to all do the same thing.  
        // NOTE: The point of this is to demonstrate the use of the different 
        // types of method references; in a normal program, you probably
        // wouldn't implement all these ways of doing the same thing
        
        // In each case we're grabbing the list's Stream object and calling
        // a series of methods on it:
        //
        // The 'map' method accepts an instance of the Function
        // interface, which is a functional interface with method signature
        // 'R apply(T t)'.  Ie, it takes an argument of one type and returns
        // a value of another (possibly the same) type (R and T are generic).
        // Effectively, map 'applies' a function to each element in the list,
        // converting each element to the value that the apply function
        // returns.
        //
        // The 'sorted' method accepts an instance of Comparator,
        // which is a functional interface with method signature
        // 'int compare(T a, T b)'.  Ie, it takes two arguments and compares
        // them, returning an int (see the notes on the compare methods above).
        // (T is again a generic type here.) The compare method will be used in
        // the sorting algorithm to determine the order of list elements.
        // The stream returned by the sorted method will be sorted according to
        // the rules specified in the comparison function that is passed in.
        //
        // The 'forEach' method accepts an instance of Consumer, which is a
        // functional interface with method signature 'void accept(T t)'.
        // Ie, it takes a parameter and does something with it.  In each case
        // below, we simply give forEach the method reference to
        // System.out.println, meaning, effectively 'for each element in the 
        // list, print out that element'
        
        
        /**************************
         * Static Method Reference
        ***************************/
        System.out.println("Using static method reference:");
        // Here, we simply pass in references to static methods on the 
        // Point class, which have the same method signatures as specified by
        // the functional interfaces in map, sorted, forEach, respectively,
        // so we can pass them in directly
        L.stream()
                .map(Point::staticReflect)
                .sorted(Point::compare)
                .forEach(System.out::println);
        
        // The equivalent code using lambdas:
        L.stream()
                .map( p -> Point.staticReflect(p) )
                .sorted( (p1, p2) -> Point.compare(p1, p2) )
                .forEach( p -> System.out.println(p) );
        
        /****************************************
         * Instance Method (of class) Reference
        *****************************************/
        System.out.println("Using instance method reference of type:");
        // The Point class's reflect method has signature 'Point reflect()'.
        // This is not the same as the Function.apply method required by map,
        // but in this case, Java will create a lambda with the Function.apply
        // signature that then calls the reflect method on the t argument 
        // that gets passed into it: t -> { return t.reflect(); }
        // The compiler determines that T in this case will be Point, because
        // L is a list of Points, hence we MUST provide a method reference to
        // a method on Point, otherwise we would get a compiler error about
        // mismatched types.
        // Similarly, compareTo method has signature 
        // 'int compareTo(Point other)' which is again different from the
        // signature specified by the functional interface that 'sorted'
        // accepts.
        // Again, Java will create a lambda that:
        // 1) has the Comparator signature
        // 2) calls the compareTo method on the first argument passed in 
        //    to the lambda, and 
        // 3) passes the remaining lambda arguments into the compareTo method
        // Effectively, Java creates the following lambda: 
        //   (a, b) -> a.compareTo(b)
        // The compiler infers all the necessary types and will complain if
        // we don't pass a reference to an instance method in Point, because
        // we are calling 'sorted' on a list of Points
        L.stream()
                .map(Point::reflect)
                .sorted(Point::compareTo)
                .forEach(System.out::println);
        
        // The equivalent code using lambdas:
        // Note how for the map and sorted lambdas, the method that we pass in
        // above by reference is called ON THE FIRST ARGUMENT THAT IS PASSED
        // INTO THE LAMBDA, and the remaining lambda arguments become the
        // arguments for the method being called on the first lambda arg.
        // When you pass a reference to an INSTANCE METHDO on a class, this is
        // the translation that Java performs for you behind the scenes.
        L.stream()
                .map( p -> p.reflect() )
                .sorted( (p1, p2) -> p1.compareTo(p2) )
                .forEach( p -> System.out.println(p) );
        
        /****************************************
         * Instance Method (of object) Reference
        *****************************************/
        System.out.println("Using instance method reference of object:");
        // In this example, we create two objects that have methods with the
        // necessary signatures, so we can pass references in directly, in this
        // case references to methods on the objects rather than the Point type
        PointComparer pc = new PointComparer();
        PointReflecter pr = new PointReflecter();
        L.stream()
                .map(pr::reflect)
                .sorted(pc::compare)
                .forEach(System.out::println);
        
        // The equivalent code using lambdas:
        L.stream()
                .map( p -> pr.reflect(p) )
                .sorted( (p1, p2) -> pc.compare(p1, p2) )
                .forEach( p -> System.out.println(p) );
        
        
        /********************************
         * Constructor Method Reference
        *********************************/
        System.out.println("Using constructor method reference:");
        // Here, we pass in a method reference to the Point constructor,
        // which as you can see above creates a new Point with the x and y
        // values swapped from the Point that was passed in.
        // The reason we can pass in this constructor reference is because
        // it effectively has the same signature as the Function
        // interface's apply method: it accepts a value (a Point) and returns 
        // another value (another Point).
        // NOTE: this constructor is probably not something you would want in
        // a real program, but it serves to demonstrate the passing of a 
        // constructor method reference.
        L.stream()
                .map(Point::new)
                .sorted(Point::compareTo)
                .forEach(System.out::println);
        
        // The equivalent code using lambdas:
        L.stream()
                .map( p -> new Point(p) )
                .sorted( (p1, p2) -> p1.compareTo(p2) )
                .forEach( p -> System.out.println(p) );
        
    }
}
