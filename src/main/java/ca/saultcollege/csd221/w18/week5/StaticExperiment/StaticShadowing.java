/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.StaticExperiment;

/**
 *
 * @author rod
 */
public class StaticShadowing {
    
    public static void main(String[] args) {
        
        Base.m();
        Sub.m();
        
        Base aSub = new Sub();
        aSub.m();  // Prints 'm on Base' even though aSub is a Sub!
                   // There is NO dynamic method binding on a static method!
    }
}
