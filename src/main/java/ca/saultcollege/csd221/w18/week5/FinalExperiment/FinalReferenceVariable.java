/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.FinalExperiment;

import java.awt.Point;
/**
 *
 * @author rod
 */
public class FinalReferenceVariable {
    
    public static void main(String[] args) {
        
        final int[] ints = new int[] { 1, 2, 3, 4, 5 };
        ints[0] = 2;  // This works!  Since ints is a reference variable, its
                      // contents can be altered even though it is final
        // ints = new int[] { 6, 7, 8 };  // This doesn't work: ints is final
        
        final Point p = new Point(100, 150);
        p.setLocation(250, 300);  // This works!  Since p is a reference
                                  // variable, its contents can be altered even
                                  // though it is final
        // p = new Point(250, 300);  // This doesn't work: p is final
        

    }
    
}
