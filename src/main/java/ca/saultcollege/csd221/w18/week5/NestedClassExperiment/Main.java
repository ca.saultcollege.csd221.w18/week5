/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.NestedClassExperiment;

import java.util.function.*;

/**
 *
 * @author rod
 */
public class Main {
    
    public static void main(String[] args) {
        // We can instantiate a static nested class directly by qualifying
        // it with its enclosing class name (Base in this case)
        // What happens if NestedBase is declared private in Base?
        Base.NestedBase b1 = new Base.NestedBase();
        b1.foo();
        
        // Calling a static method in a static nested class
        Base.NestedBase.s();  

        // We can't instantiate inner classes in the same way as nested classes,
        // though.  We need an instance of Base first.
        // Base.InnerBase b2 = new Base.InnerBase();  // won't compile
        Base b = new Base();
        Base.InnerBase b2 = b.new InnerBase();
        b2.bar();
        System.out.println(b2.CONST);  // Accessing constant in inner class
        
        // Call a method that uses a local class
        b.useLocalClass("hi");
        
        // Use an anonymous inner class
        Base base = new Base() {
            // Here, we create a new anonymous class that overrides the
            // useLocalClass method in Base
            @Override
            public void useLocalClass(String incomingStr) {
                System.out.println("This is different:" + incomingStr);
            }
        };
        base.useLocalClass("hi");
        
        // We can also use anonymous inner classes to make single-use
        // implemetations of interfaces.  This can be useful if you really only
        // need an implementation for one place in your code, and it doesn't
        // seem worth writing a whole separate file to get the implementation.
        Supplier<String> stringSupplier = new Supplier<String>() {
            @Override
            public String get() {
                return "this is the string that was supplied";
            }
        };
        System.out.println(stringSupplier.get());
    }
    
}
