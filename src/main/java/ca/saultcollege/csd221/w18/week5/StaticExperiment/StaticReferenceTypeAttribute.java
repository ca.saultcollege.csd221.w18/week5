/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.StaticExperiment;

import java.util.ArrayList;

/**
 *
 * @author rod
 */
public class StaticReferenceTypeAttribute {
    
    // If we want list to be populated with some values by default, we can't
    // completely initialize it here...
    public static ArrayList<Integer> list = new ArrayList<>();
    
    // ...but we can use the static initializer to do so
    // Think of the static initializer as a 'constructor' for static attributes
    static {
        list.add(1);
        list.add(2);
        list.add(3);
    }
    
    public static void main(String[] args) {
        
        // What happens if you comment out the static initializer?
        System.out.println(StaticReferenceTypeAttribute.list.get(2));
    }
}
