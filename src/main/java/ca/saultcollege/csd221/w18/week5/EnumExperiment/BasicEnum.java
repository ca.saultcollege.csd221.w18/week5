/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.EnumExperiment;

/**
 * Copied from https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
 */
public class BasicEnum {
    enum Day { SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY; }
    
    Day day;
    
    public BasicEnum(Day day) {
        this.day = day;
    }
    
    public void tellItLikeItIs() {
        switch (day) {
            case MONDAY:
                System.out.println("Mondays are bad.");
                break;
                    
            case FRIDAY:
                System.out.println("Fridays are better.");
                break;
                         
            case SATURDAY: case SUNDAY:
                System.out.println("Weekends are best.");
                break;
                        
            default:
                System.out.println("Midweek days are so-so.");
                break;
        }
    }
    
    public static void main(String[] args) {
        BasicEnum firstDay = new BasicEnum(Day.MONDAY);
        firstDay.tellItLikeItIs();
        BasicEnum thirdDay = new BasicEnum(Day.WEDNESDAY);
        thirdDay.tellItLikeItIs();
        BasicEnum fifthDay = new BasicEnum(Day.FRIDAY);
        fifthDay.tellItLikeItIs();
        BasicEnum sixthDay = new BasicEnum(Day.SATURDAY);
        sixthDay.tellItLikeItIs();
        BasicEnum seventhDay = new BasicEnum(Day.SUNDAY);
        seventhDay.tellItLikeItIs();
    }
}