/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.EnumExperiment;

/**
 * Copied from https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
 */
public enum Planet {
    
    // enum values can be declared with values to pass into the constructor
    MERCURY (3.303e+23, 2.4397e6),
    VENUS   (4.869e+24, 6.0518e6),
    EARTH   (5.976e+24, 6.37814e6),
    MARS    (6.421e+23, 3.3972e6),
    JUPITER (1.9e+27,   7.1492e7),
    SATURN  (5.688e+26, 6.0268e7),
    URANUS  (8.686e+25, 2.5559e7),
    NEPTUNE (1.024e+26, 2.4746e7);

    // enums can have attributes
    private final double mass;   // in kilograms
    private final double radius; // in meters

    // enums can have static attributes
    // universal gravitational constant  (m3 kg-1 s-2)
    public static final double G = 6.67300E-11;
    
    // enums can have non-default constructors
    // (But they will always be private and cannot be called anywhere aside
    // from in the enum value declarations above)
    Planet(double mass, double radius) {
        this.mass = mass;
        this.radius = radius;
    }
    
    // enums can have methods
    private double mass() { return mass; }
    private double radius() { return radius; }

    double surfaceGravity() {
        return G * mass / (radius * radius);
    }
    double surfaceWeight(double otherMass) {
        return otherMass * surfaceGravity();
    }
    
    public static void main(String[] args) {
    
        // your weight on earth
        double earthWeight = 175.0;
        
        double mass = earthWeight/EARTH.surfaceGravity();
        
        // Example use of the values() method
        for (Planet p : Planet.values())
            // Example of automatic conversion of enum values to string
            // and calling of method on enum value
            System.out.printf("Your weight on %s is %f%n",
                             p, p.surfaceWeight(mass));
    }
}
