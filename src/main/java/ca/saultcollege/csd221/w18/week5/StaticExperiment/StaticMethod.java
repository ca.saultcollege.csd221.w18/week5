/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.StaticExperiment;

/**
 *
 * @author rod
 */
public class StaticMethod {
    
    public static int staticInt;
    public int regularInt;
    
    public static void m(String s) { 
        System.out.println(s); 
        
        // Can refer to static attributes from within static methods
        System.out.println(staticInt); 
        // CANNOT refer to non-static attributes from within static methods
        // System.out.println(regularInt); 
    }
    
    public void n() {
        // Can refer to static attributes from within non-static methods
        System.out.println(staticInt); 
        // Can refer to non-static attributes from within non-static methods
        System.out.println(regularInt); 
    }
    
    
    public static void main(String[] args) {

        // m can be called directly on the class name
        StaticMethod.m("Hello, world!");
        
        // m can be called on an object instance, but it's the same as calling
        // it on the class
        StaticMethod obj = new StaticMethod();
        // These two lines are equivalent:
        obj.m("Hello, object!");
        StaticMethod.m("Hello, object!");
    }
}
