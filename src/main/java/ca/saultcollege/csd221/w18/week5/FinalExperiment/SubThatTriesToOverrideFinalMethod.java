/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.FinalExperiment;

/**
 *
 * @author rod
 */
public class SubThatTriesToOverrideFinalMethod extends Base {
    
      // This method will not compile because m() is declared as final in Base
//    @Override
//    public void m() {
//        System.out.println("SubThatTriesToOverrideFinalMethod.m()");
//    }
}
