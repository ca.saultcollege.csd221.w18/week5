/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.LambdaExperiment;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.function.Function;
import java.util.function.BiFunction;
import java.util.function.Consumer;

/**
 *
 * @author rod
 */
public class Main {
    
    public static String addPrefix(String s) {
        return "Prefix from existing Method: " + s;
    }
    
    public static boolean startsWithAbc(String s) {
        return s.startsWith("abc");
    }
    
    public static void main(String[] args) {
        
        Map<String, Integer> m = new HashMap<>();
        m.put("Trump", 36);
        
        // Using an Anonymous Class
        m.merge("Trump", 4, new BiFunction<Integer,Integer,Integer>() {
            @Override
            public Integer apply(Integer x, Integer y) {
                    return x + y;
            }
        }); 
        
        // Since we have a single-method (ie, 'functional') interface 
        // we can use a lambda expression
        m.merge("Trump", 4, (Integer x, Integer y) -> { return x + y; });

        // This line does the same thing as both the lambda expression above
        // and the Anonymous class above.  The compiler can infer what
        // types x and y need to be, and we don't need the curly braces or
        // return keyword in the case of single-statement lambda bodies
        m.merge("Trump", 4, (x, y) -> x + y);

        
        List<String> L = new ArrayList<>();
        L.add("abcde");
        L.add("edcba");
        L.add("abcxy");
        L.add("xyz");
        L.add("abc");
        
        // Do some stuff using Anonymous Classes
        L.stream()
            .map(new Function<String, String>(){
                @Override
                public String apply(String s) {
                    return "prefix1:" + s;
                }
            })
            .forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                    System.out.println(s);
                }
            });
        
        // Do the same thing using lambda expressions
        L.stream()    
            // pick only elements that start with 'abc'
            .filter( s -> s.startsWith("abc") )
            // add a prefix to each element
            .map( s -> "prefix2:" + s )
            // print each element
            .forEach( s -> System.out.println(s) );
            // Note above:
            // We don't even need brackets around the parameter list for single-
            // parameter lambdas
            
            
        // Do the same thing using method references
        L.stream()
            .filter(Main::startsWithAbc)
            .map(Main::addPrefix)
            .forEach(System.out::println);
        
        
        int x = 1;
        Consumer<Integer> c = i -> {
            
            // We have access to 'effectively final' local variables here
            System.out.println(x);
            System.out.println(i);
            
            // x = 2;  // won't compile because x has already been assigned
        };
        
        // x = 2;  // What happens if we uncomment this line?
        
    }
    
}
