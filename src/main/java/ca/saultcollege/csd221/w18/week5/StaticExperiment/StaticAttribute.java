/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.StaticExperiment;

/**
 *
 * @author rod
 */
public class StaticAttribute {
    
    public static int x = 0;
    
    public static void main(String[] args) {
        
        // static int y = 1;  // Won't compile: a plain (non-field/attribute)
                              // variable can't be static
                
        // We can access x without instantiating a StaticAttribute object
        // because it is static
        System.out.println(StaticAttribute.x); 
        StaticAttribute.x = 1;
        System.out.println(StaticAttribute.x);
        
        StaticAttribute s1 = new StaticAttribute();
        StaticAttribute s2 = new StaticAttribute();
        
        // Setting a static attribute changes the value across the whole class
        // (Any other object referring to it will have the same value)
        s1.x = 2;
        System.out.println(s2.x);
        System.out.println(StaticAttribute.x);
    }
}
