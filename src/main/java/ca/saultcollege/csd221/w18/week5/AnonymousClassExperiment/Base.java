/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.AnonymousClassExperiment;

import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author rod
 */
public class Base {
    
    private int x = 1;
    
    public void m() {
        
        int y = 1;
        int z = 1;
        
        JButton btn = new JButton();
        
        // Instead of defining a class that implements ActionListener in a
        // separate file, we can just creat an anonymous class here
        btn.addActionListener(new ActionListener() {
            // Inside of these curly braces, it's just a normal class definition
            
            private int p;            // We can define non-static attributes
            // private static int s;  // But static attributes are not allowed
            
            // final static variables are OK, though
            public final static int CONST = 1;
            
            // We can define non-static methods
            public void m() { System.out.println("m"); }
            
            // But static methods are not allowed
            // public static void n() { System.out.println("m"); }
            
            // We can override methods in the class we are implementing or
            // extending, in this case the Interface ActionListener.
            // What happens if we comment out his method?
            @Override
            public void actionPerformed(ActionEvent e) {
                x = 2;    // We can assign to x because it is an attribute
                          // of the enclosing class, not simply a local variable
                          
                // y = 1; // We can't assign to y because local variables that
                          // an anonymous class refers to must be effectively
                          // final

                // Since z is effectively final, we can refer to it inside this
                // anonymous class
                System.out.println(z);
            }
        });
    }
    
}
