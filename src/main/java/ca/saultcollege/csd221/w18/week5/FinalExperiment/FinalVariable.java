/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.FinalExperiment;

/**
 *
 * @author rod
 */
public class FinalVariable {
    
    public static void main(String[] args) {
        
        final int x = 1;
        // x = 2;  // Will not compile because x is final 
                   // AND has already been assigned
                   
        final int y;
        y = 1;     // Compiles because y has not yet been assigned
        // y = 2;  // Will not compile because y is final
                   // AND has already been assigned
        
    }
    
}
