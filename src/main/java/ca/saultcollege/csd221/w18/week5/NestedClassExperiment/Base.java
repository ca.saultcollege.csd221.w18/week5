/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week5.NestedClassExperiment;

/**
 *
 * @author rod
 */
public class Base {
    
    private static int staticX = 99;
    private int x = 100;
    
    // What happens when we set this class to private or protected?
    public static class NestedBase {
        public void foo() {
            // The static nested class has access to only static
            // attributes of the enclosing class
            // System.out.println(x);  // won't compile: x is not static
            System.out.println(staticX);
        }
        
        // Static nested classes can have static methods
        public static void s() {
            System.out.println("static method");
        }
    }
    
    // What happens when we set this class to private or protected?
    public class InnerBase {
        // This is ok: inner classes CAN have static final variables
        public static final int CONST = 1;
        
        public void bar() {
            // The inner class has access to both static and non-static
            // attributes of the enclosing class
            System.out.println(x);
            System.out.println(staticX);
        }
        
        // Won't compile: inner classes cannot have static methods
//        public static void s() {
//            System.out.println("static method");
//        }
    }
    
        
    public void useLocalClass(String incomingStr) {
        
        String localStr = "local string";
        
        // Here is a local class.  Notice the lack of visibility modifier.
        // (Local classes are always only accessible to their enclosing scope,
        // in this case the 'main' method that we're in.)
        class Local {
            
            public void buz() { 
                // In addition to both the static and non-static members of the
                // enclosing class, Local Classes have access to variables in
                // the enclosing scope (with a caveat)!
                System.out.println("From local class: x=" + x);
                System.out.println("From local class: staticX=" + staticX);
                System.out.println("From local class: incomingStr=" + incomingStr);
                System.out.println("From local class: localStr=" + localStr);
                
                // CAVEAT: what happens if we change a variable in the local
                // scope?  Uncomment the line below to find out:
                // 
                // localStr = "something else";
                //
                // Because of the way that local classes 'capture' the values
                // of the locally scoped variables, any locally scoped variable
                // must be 'effectively final', that is, it may not be changed
                
                // Changing non-locally scoped variables (ie, variables from 
                // the enclosing CLASS rather than the enclosing METHOD may
                // be changed, though:
                x = 1;
                staticX = 2;
            }
        }
        
        // The CAVEAT above applies even outside the local class.  Any locally
        // scoped variable used in the local class must be effectively final.
        // Eg. try uncommenting the line below:
        //
        // localStr = "something else";
        
        
        Local L = new Local();
        L.buz();
        
    }
    
}
